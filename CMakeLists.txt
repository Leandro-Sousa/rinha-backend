cmake_minimum_required(VERSION 3.15)

project(rinha-backend VERSION 1.0 
                      DESCRIPTION "A simple project for rinha de backends competition." 
                      LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS_RELEASE "-Wall -Wextra -O3")

include_directories(include ${PROJECT_SOURCE_DIR})

add_executable(${PROJECT_NAME} 
               apps/domain/entities/person.cpp
               apps/domain/repositories/person_repository.hpp
               apps/adapters/person_adapter.cpp
               apps/application/common/result.cpp
               apps/application/person_service.cpp
               apps/ports/repositories/databases/postgres_person_repository.cpp
               apps/ports/http/server/common/validation_result.cpp
               apps/ports/http/server/validators/person_body_validator.cpp
               apps/ports/http/server/controllers/person_controller.cpp
               apps/main.cpp
)

find_package(PostgreSQL REQUIRED)
add_library(PostgreSQL::client INTERFACE IMPORTED)

find_package(Poco REQUIRED COMPONENTS Foundation JSON Data DataPostgreSQL)

find_package(Drogon CONFIG REQUIRED)

target_link_libraries(${PROJECT_NAME} PRIVATE Drogon::Drogon
                                      PRIVATE Poco::Foundation 
                                      PRIVATE Poco::JSON
                                      PRIVATE Poco::Data 
                                      PUBLIC Poco::DataPostgreSQL
                                      PostgreSQL::client INTERFACE PostgreSQL::PostgreSQL)
