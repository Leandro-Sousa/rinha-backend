CREATE TABLE pessoas
(
    id UUID PRIMARY KEY
    , apelido VARCHAR(32) NOT NULL
    , nome VARCHAR(100) NOT NULL
    , data_nascimento DATE NOT NULL
    , stack VARCHAR(50) ARRAY NULL
    , termos_pesquisa TEXT NOT NULL
);

CREATE UNIQUE INDEX pessoas_apelido_idx ON pessoas(apelido);

CREATE EXTENSION pg_trgm;
CREATE EXTENSION btree_gist;

CREATE INDEX CONCURRENTLY pessoas_termos_pesquisa_idx ON pessoas USING GIST(termos_pesquisa gist_trgm_ops);

CREATE OR REPLACE PROCEDURE insert_pessoa
(
    p_id              pessoas.id%TYPE,
    p_apelido         pessoas.apelido%TYPE,
    p_nome            pessoas.nome%TYPE,
    p_data_nascimento pessoas.data_nascimento%TYPE,
    p_stack           pessoas.stack%TYPE,
    p_termos_pesquisa pessoas.termos_pesquisa%TYPE
)
LANGUAGE plpgsql
AS $$
BEGIN
    INSERT INTO public.pessoas(id  , apelido  , nome  , data_nascimento  , stack  , termos_pesquisa) 
    VALUES                    (p_id, p_apelido, p_nome, p_data_nascimento, p_stack, p_termos_pesquisa);
END; $$;

CREATE OR REPLACE FUNCTION get_pessoa_by_id(p_id pessoas.id%TYPE)
RETURNS TABLE (
    apelido         pessoas.apelido%TYPE,
    nome            pessoas.nome%TYPE,
    data_nascimento pessoas.data_nascimento%TYPE,
    stack           pessoas.stack%TYPE
)
LANGUAGE plpgsql
PARALLEL SAFE
ROWS 1
IMMUTABLE
AS $$
BEGIN
    RETURN QUERY 
    SELECT 
        p.apelido
        , p.nome
        , p.data_nascimento
        , p.stack 
    FROM public.pessoas AS p 
    WHERE p.id = p_id;
END; $$;

CREATE OR REPLACE FUNCTION count_by_nickname(p_apelido pessoas.apelido%TYPE)
RETURNS INTEGER
LANGUAGE plpgsql
PARALLEL SAFE
IMMUTABLE
AS $$
DECLARE count INTEGER;
BEGIN
    SELECT 
        COUNT(1) INTO count 
    FROM public.pessoas 
    WHERE apelido = p_apelido;
    RETURN count;
END; $$;

CREATE OR REPLACE FUNCTION get_pessoas_by_termo(p_termo pessoas.termos_pesquisa%TYPE)
RETURNS TABLE (
    id              pessoas.id%TYPE,
    apelido         pessoas.apelido%TYPE,
    nome            pessoas.nome%TYPE,
    data_nascimento pessoas.data_nascimento%TYPE,
    stack           pessoas.stack%TYPE
)
LANGUAGE plpgsql
PARALLEL SAFE
ROWS 50
STABLE
AS $$
BEGIN
    RETURN QUERY 
    SELECT 
        p.id
        , p.apelido
        , p.nome
        , p.data_nascimento
        , p.stack 
    FROM public.pessoas AS p
    WHERE p.termos_pesquisa ILIKE '%' || p_termo || '%' 
    LIMIT 50;
END; $$;
