# Rinha de Backend in C++

## Dependencies

- Poco 1.12
- drogonframework 1.9.0

## Create docker image

```sh
docker build -t leandro-sousa/rinha-backend . 
```

## Start infrastructure 

```sh
docker compose up -d
```
