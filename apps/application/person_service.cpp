#pragma once

#include <memory>

#include <apps/domain/entities/person.cpp>
#include <apps/domain/repositories/person_repository.hpp>
#include <apps/application/common/result.cpp>

class PersonService
{
    public: 
    
        PersonService(const PersonRepositoryPtr& personRepository) : _personRepository(personRepository)
        {
        }

        ~PersonService()
        {
        }

        Result<PersonPtr> create(const PersonPtr& person)
        {
            auto nicknameAlreadyExists = this->_personRepository->existsByNickname(person->getNickname());

            if(nicknameAlreadyExists)
            {
                return Result<PersonPtr>::fail("nickname already exists.");
            }

            this->_personRepository->insert(person);
             
            return Result<PersonPtr>::success(person);
        }

        std::optional<PersonPtr> getById(const Poco::UUID& id)
        {
            return this->_personRepository->getById(id);
        }

        PersonListPtr getByTerm(const std::string& term)
        {
            return this->_personRepository->getByTerm(term);
        }

        std::uint32_t count()
        {
            return this->_personRepository->count();
        }

    private:
        PersonRepositoryPtr _personRepository;
};

using PersonServicePrt = std::shared_ptr<PersonService>;
