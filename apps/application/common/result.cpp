#include <optional>
#include <string>

template<typename T>
class Result
{
    public:
        static Result<T> success(const T& data)
        {
            return Result(data);
        }

        static Result<T> fail(const std::string& failMessage)
        {
            return Result(failMessage);
        }

        bool isSuccess() const
        {
            return !this->isFail();
        }

        bool isFail() const
        {
            return this->_failMessage.has_value();
        }

        T getData() const
        {
            return this->_data.value();
        }

        std::string getFailMessage() const
        {
            return this->_failMessage.value();
        }

    private:
        std::optional<T> _data;
        std::optional<std::string> _failMessage;

        Result(const T& data): _data(data), _failMessage(std::nullopt)
        {
        }

        Result(const std::string &failMessage): _data(std::nullopt), _failMessage(failMessage)
        {
        }
};
