#include <iostream>
#include <format>
#include <memory>

#include <drogon/drogon.h>

#include <Poco/Data/SessionPool.h>
#include <Poco/Data/PostgreSQL/Connector.h>

#include "application/person_service.cpp"
#include "ports/http/server/controllers/person_controller.cpp"
#include "ports/repositories/databases/postgres_person_repository.cpp"
#include "domain/repositories/person_repository.hpp"

int main(int argc, char *argv[])
{
	Poco::Data::PostgreSQL::Connector::registerConnector();

	auto connectionString = std::format("host={0} port=5432 dbname={1} user={2} password={3}",
										std::getenv("DATABASE_HOST"),
										std::getenv("DATABASE_NAME"),
										std::getenv("DATABASE_USER"),
										std::getenv("DATABASE_PASSWORD"));

	auto sessionPool = std::make_shared<Poco::Data::SessionPool>(
		Poco::Data::PostgreSQL::Connector::KEY, 
		connectionString, 
		8, 
		40
	);

	auto personService = std::make_shared<PersonService>(
		std::make_shared<PostgresPersonRepository>(sessionPool)
	);

	auto port = std::stoi(std::getenv("APP_PORT"));
	drogon::app()
		.setLogLevel(trantor::Logger::kInfo)
		.addListener("0.0.0.0", port)
		.setThreadNum(6)
		.registerController(std::make_shared<PersonController>(personService))
		.run();
}
