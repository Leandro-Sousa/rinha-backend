#pragma once

#include <iostream>

#include <Poco/JSON/Parser.h>
#include <Poco/UUID.h>

#include <apps/domain/entities/person.cpp>

class PersonAdapter
{
    public:
        static PersonPtr fromJson(const std::string& json)
        {
            Poco::JSON::Parser parser;
			auto jsonResult = parser.parse(json);
			auto jsonObject = jsonResult.extract<Poco::JSON::Object::Ptr>();
            return fromJson(jsonObject);
        }

        static PersonPtr fromJson(const Poco::JSON::Object::Ptr& json)
        {
            auto id = json->getValue<Poco::UUID>("id");
            auto nickname = json->getValue<std::string>("apelido");
            auto name = json->getValue<std::string>("nome");
            auto birthDate = json->getValue<Poco::DateTime>("nascimento");
            auto stackValues = parseStack(json->getArray("stack"));

            return std::make_shared<Person>(id, nickname, name, birthDate, stackValues);
        }

        static PersonPtr createNewFromJson(const Poco::JSON::Object::Ptr& json)
        {
            auto nickname = json->getValue<std::string>("apelido");
            auto name = json->getValue<std::string>("nome");
            auto birthDate = json->getValue<Poco::DateTime>("nascimento");
            auto stackValues = parseStack(json->getArray("stack"));

            return Person::createNew(nickname, name, birthDate, stackValues);
        }

        static std::string toJsonString(const PersonPtr& person)
        {
			auto json = toJsonObject(person);
            std::ostringstream output;
            json.stringify(output);
			return output.str();
        }

        static Poco::JSON::Object toJsonObject(const PersonPtr& person)
        {
			Poco::JSON::Object json;
			json.set("id", person->getId().toString());
			json.set("apelido", person->getNickname());
			json.set("nome", person->getName());
			json.set("nascimento", Poco::DateTimeFormatter::format(person->getBirthDate(), "%Y-%m-%d"));
			auto stack = person->getStack();
			if(stack.has_value())
			{
				Poco::JSON::Array array;
				for(auto value : stack.value())
				{
					array.add(value);
				}
				json.set("stack", array);
			}
            return json;
        }

    private:
    	static Stack parseStack(Poco::JSON::Array::Ptr array)
		{
			if(array == nullptr || array.isNull())
			{
				return std::nullopt;
			}

			std::vector<std::string> stack{};
			for(auto it = array->begin(); it != array->end(); ++it)
			{
				if(!it->isEmpty())
				{
					stack.push_back(it->convert<std::string>());
				}
			}
			return std::optional(stack);
		}
};
