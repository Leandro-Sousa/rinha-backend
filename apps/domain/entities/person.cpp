#pragma once

#include <iostream>
#include <optional>
#include <string>

#include <Poco/DateTime.h>
#include <Poco/UUID.h>
#include <Poco/UUIDGenerator.h>

class Person;

using Stack = std::optional<std::vector<std::string>>;
using PersonPtr = std::shared_ptr<Person>;
using PersonListPtr = std::shared_ptr<std::vector<PersonPtr>>;

class Person
{
    public:
        Person( const Poco::UUID& id, 
                const std::string& nickname, 
                const std::string& name, 
                const Poco::DateTime birthDate, 
                Stack stack) 
        : _id(id), _nickname(nickname), _name(name), _birthDate(birthDate), _stack(stack)
        {
        }

        const Poco::UUID& getId() const 
        {
            return this->_id;
        }

        const std::string& getNickname() const
        {
            return this->_nickname;
        }

        const std::string& getName() const
        {
            return this->_name;
        }

        const Poco::DateTime& getBirthDate() const
        {
            return this->_birthDate;
        }

        const Stack& getStack() const
        {
            return this->_stack;
        }

        static PersonPtr createNew(const std::string& nickname, const std::string& name, const Poco::DateTime& bithDate, const Stack& s)
        {
            static Poco::UUIDGenerator generator;

            auto id = generator.createRandom();

            return std::make_shared<Person>(id, nickname, name, bithDate, std::move(s));
        }
    
    private:
        Poco::UUID _id;
        std::string _nickname;
        std::string _name;
        Poco::DateTime _birthDate;
        Stack _stack;
};
