#pragma once

#include "apps/domain/entities/person.cpp"

class PersonRepository
{
    public:
        virtual void insert(const PersonPtr& person) = 0;
        virtual bool existsByNickname(const std::string& nickname) = 0;
        virtual std::optional<PersonPtr> getById(const Poco::UUID& id) = 0;
        virtual PersonListPtr getByTerm(const std::string& term) = 0;
        virtual std::uint32_t count() = 0;
};

using PersonRepositoryPtr = std::shared_ptr<PersonRepository>;
