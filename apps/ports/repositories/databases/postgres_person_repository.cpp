#include <functional>
#include <iostream>
#include <sstream>

#include <Poco/DateTimeFormatter.h>
#include <Poco/Data/RecordSet.h>
#include <Poco/Data/Session.h>
#include <Poco/Data/SessionPool.h>
#include <Poco/Data/PostgreSQL/Connector.h>

#include <apps/domain/entities/person.cpp>
#include <apps/domain/repositories/person_repository.hpp>

class PostgresPersonRepository : public PersonRepository
{
    public:
        PostgresPersonRepository(const std::shared_ptr<Poco::Data::SessionPool>& sessionPool) : _sessionPool(sessionPool)
        {

        }

        void insert(const PersonPtr& person) override
        {
            try
            {
                auto session = this->_sessionPool->get();
                auto id = person->getId();
                auto nickname = person->getNickname();
                auto name = person->getName();
                auto birthDate = person->getBirthDate();
                auto stack = this->vectorToArray(person->getStack());
                auto searchTerms = this->createSearchTems(person);

                session << "CALL insert_pessoa($1, $2, $3, $4, $5, $6);", 
                               Poco::Data::Keywords::use(id), 
                               Poco::Data::Keywords::use(nickname), 
                               Poco::Data::Keywords::use(name), 
                               Poco::Data::Keywords::use(birthDate), 
                               Poco::Data::Keywords::use(stack),
                               Poco::Data::Keywords::use(searchTerms),
                               Poco::Data::Keywords::now;
            }                                           
            catch(const Poco::Exception& e)
            {
                std::cerr << "[PostgresPersonRepository::insert]" << e.displayText() << '\n';
            }
        }

        bool existsByNickname(const std::string& nickname) override
        {
            int result = 0;
            try
            {
                auto session = this->_sessionPool->get();
                Poco::Data::Statement statement(session);
                statement << "SELECT count_by_nickname($1);",
                            Poco::Data::Keywords::use(const_cast<std::string&>(nickname)),
                            Poco::Data::Keywords::into(result),
                            Poco::Data::Keywords::now;
            }
            catch(const Poco::Exception& e)
            {
                std::cerr << "[PostgresPersonRepository::existsByNickname]" << e.displayText() << '\n';
            }

            return static_cast<bool>(result);
        }

        std::optional<PersonPtr> getById(const Poco::UUID& id) override
        {
            try
            {
                auto session = this->_sessionPool->get();
                Poco::Data::Statement statement(session);
                std::string nickname;
                std::string name;
                Poco::DateTime birthDate;
                Poco::Nullable<std::string> stack;
                statement << "SELECT * FROM get_pessoa_by_id($1);",
                             Poco::Data::Keywords::into(nickname),
                             Poco::Data::Keywords::into(name),
                             Poco::Data::Keywords::into(birthDate),
                             Poco::Data::Keywords::into(stack),
                             Poco::Data::Keywords::use(const_cast<Poco::UUID&>(id));

                auto rowCount = statement.execute();

                if(rowCount == 1)
                {
                    auto stackValues = this->arrayToVector(stack);

                    return std::optional(std::make_shared<Person>(id, nickname, name, birthDate, stackValues));
                }
            }
            catch(const Poco::Exception& e)
            {
                std::cerr << "[PostgresPersonRepository::getById]" << e.displayText() << '\n';
            }

            return std::nullopt;
        }

        PersonListPtr getByTerm(const std::string& term) override
        {
            auto result = std::make_shared<std::vector<PersonPtr>>();
            try
            {
                auto session = this->_sessionPool->get();
                Poco::Data::Statement statement(session);
                Poco::UUID id;
                std::string nickname;
                std::string name;
                Poco::DateTime birthDate;
                Poco::Nullable<std::string> stack;
                statement << "SELECT * FROM get_pessoas_by_termo($1);",
                             Poco::Data::Keywords::into(id),
                             Poco::Data::Keywords::into(nickname),
                             Poco::Data::Keywords::into(name),
                             Poco::Data::Keywords::into(birthDate),
                             Poco::Data::Keywords::into(stack),
                             Poco::Data::Keywords::use(const_cast<std::string&>(term)),
                             Poco::Data::Keywords::range(0, 1);

                while (!statement.done())
                {
                    auto rowCount = statement.execute();

                    if(rowCount > 0)
                    {
                        auto stackValues = this->arrayToVector(stack);

                        result->push_back(std::make_shared<Person>(id, nickname, name, birthDate, stackValues));
                    }
                }
            }
            catch(const Poco::Exception& e)
            {
                std::cerr << "[PostgresPersonRepository::getByTerm]" << e.displayText() << '\n';
            }   

            return (result);
        }

        std::uint32_t count() override
        {
            std::uint32_t result = 0;
            try
            {
                auto session = this->_sessionPool->get();
                Poco::Data::Statement statement(session);
                statement << "SELECT COUNT(*) FROM public.pessoas;",
                            Poco::Data::Keywords::into(result),
                            Poco::Data::Keywords::now;
            }
            catch(const Poco::Exception& e)
            {
                std::cerr << "[PostgresPersonRepository::count]" << e.displayText() << '\n';
            }

            return result;   
        }

    private:
        std::shared_ptr<Poco::Data::SessionPool> _sessionPool;

        const Poco::Nullable<std::string> vectorToArray(const std::optional<std::vector<std::string>>& stringVector)
        {
            if(!stringVector.has_value())
            {
                return Poco::Nullable<std::string>();
            }

            auto values = stringVector.value();
            std::stringstream terms;

            terms << "{";
            if(!values.empty())
            {
                bool shouldAddSeparator = false;
                for(auto& value : values)
                {
                    if(shouldAddSeparator)
                    {
                        terms << ",";
                    }
                    else 
                    {
                        shouldAddSeparator = true;
                    }

                    terms << "\"" << value << "\"";
                }
            }
            terms << "}";

            return Poco::Nullable<std::string>(terms.str());
        }

        const std::optional<std::vector<std::string>> arrayToVector(const Poco::Nullable<std::string>& stringArray)
        {
            if(stringArray.isNull())
            {
                return std::nullopt;
            }
            auto arrayValue = stringArray.value();
            
            std::istringstream is(arrayValue.substr(1, arrayValue.size() - 2));
            std::string token;
            std::vector<std::string> values;

            while(std::getline(is, token, ','))
            {
                values.push_back(token);
            }
            return std::optional(values);
        }

        const std::string createSearchTems(const PersonPtr& person)
        {
            std::stringstream terms;

            terms << person->getNickname() << " " << person->getName();
            auto stack = person->getStack();
            if(stack.has_value())
            {
                for(auto& stack : stack.value()) 
                {
                    terms << " " << stack;
                }
            }

            return terms.str();
        }
};

using PostgresPersonRepositoryPtr = std::shared_ptr<PostgresPersonRepository>;
