#include <string>

#include <Poco/JSON/Parser.h>

#include <apps/ports/http/server/common/validation_result.cpp>

class PersonBodyValidator
{
	public:
		ValidationResult validate(const Poco::JSON::Object::Ptr& body)
		{
			if(body->isNull("nome"))
			{
				return ValidationResult::error("nome field is required.");
			}

			if(body->isNull("apelido"))
			{
				return ValidationResult::error("apelido field is required.");
			}

			auto name = body->getValue<std::string>("nome");
			auto nickname = body->getValue<std::string>("apelido");

			if(name.size() > 100)
			{
				return ValidationResult::error("name length cannot be greater than 100.");
			}

			if(nickname.size() > 32)
			{
				return ValidationResult::error("nickname length cannot be greater than 32.");
			}

			if(body->isNull("nascimento"))
			{
				return ValidationResult::error("nascimento field is required.");
			}

			auto rawBirthDate = body->getValue<std::string>("nascimento");
			int timeZoneDifferential = 0;
			Poco::DateTime birthDate;

			if(!Poco::DateTimeParser::tryParse(rawBirthDate, birthDate, timeZoneDifferential))
			{
				return ValidationResult::error("nascimento has invalid date format.");
			}

			if(!body->isNull("stack"))
			{
				if(!body->isArray("stack"))
				{
					return ValidationResult::error("stack must be an array.");
				}

				auto stackArray = body->getArray("stack");
				for(auto it = stackArray->begin(); it != stackArray->end(); ++it)
				{
					if(!it->isEmpty() && it->isString())
					{
						auto value = it->convert<std::string>();
						if(value.size() > 32)
						{
							return ValidationResult::error("stack item length cannot be greater than 32.");
						}
					}
				}
			}

			return ValidationResult::success();
		}
};
