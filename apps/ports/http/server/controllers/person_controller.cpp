#include <algorithm>
#include <iostream>
#include <format>
#include <memory>
#include <optional>

#include <drogon/drogon.h>
#include <drogon/HttpController.h>
#include <Poco/JSON/Parser.h>

#include <apps/adapters/person_adapter.cpp>
#include <apps/ports/http/server/common/validation_result.cpp>
#include <apps/ports/http/server/validators/person_body_validator.cpp>
#include <apps/application/person_service.cpp>

class PersonController: public drogon::HttpController<PersonController, false>
{
	public:
		METHOD_LIST_BEGIN
        ADD_METHOD_TO(PersonController::create, "/pessoas", drogon::Post);
		ADD_METHOD_TO(PersonController::getById, "/pessoas/{id}", drogon::Get);
		ADD_METHOD_TO(PersonController::getByTerm, "/pessoas?t={term}", drogon::Get);
		ADD_METHOD_TO(PersonController::count, "/contagem-pessoas", drogon::Get);
		METHOD_LIST_END

        void create(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback) const {
			auto resp = drogon::HttpResponse::newHttpResponse();
			auto contentType = req->getContentType();
			if(contentType == drogon::ContentType::CT_APPLICATION_JSON)
			{
				auto body = req->getBody();

				try
				{			
					Poco::JSON::Parser parser;
					auto jsonResult = parser.parse({body.begin(), body.end()});
					auto jsonObject = jsonResult.extract<Poco::JSON::Object::Ptr>();

					static PersonBodyValidator validator;
					auto validationResult = validator.validate(jsonObject);

					if(validationResult.hasError())
					{
						resp->setStatusCode(drogon::HttpStatusCode::k422UnprocessableEntity);
						resp->setBody(validationResult.getErrorMessage());
						callback(resp);
						return;
					}

					auto person = PersonAdapter::createNewFromJson(jsonObject);

					auto createResult = this->_personService->create(person);

					if(createResult.isSuccess())
					{
						auto person = createResult.getData();
						resp->setStatusCode(drogon::HttpStatusCode::k201Created);
						resp->addHeader("Location", "/pessoas/" + person->getId().toString());
					}
					else
					{
						resp->setStatusCode(drogon::HttpStatusCode::k422UnprocessableEntity);
					}
				}
				catch(const std::exception& e)
				{
					std::cerr << e.what() << std::endl;
					std::cerr << body << std::endl;
					resp->setStatusCode(drogon::HttpStatusCode::k500InternalServerError);
				}
			} 
			else 
			{
				resp->setStatusCode(drogon::HttpStatusCode::k400BadRequest);
			}

			callback(resp);
        }

		void getById(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback, const std::string& id) const {
			Poco::UUID parsedId(id);
			auto person = this->_personService->getById(parsedId);
			auto resp = drogon::HttpResponse::newHttpResponse();
			if(person.has_value())
			{
				auto json = PersonAdapter::toJsonString(person.value());
				resp->setStatusCode(drogon::HttpStatusCode::k200OK);
				resp->setContentTypeCode(drogon::ContentType::CT_APPLICATION_JSON);
				resp->setBody(json);
			}
			else
			{
				resp->setStatusCode(drogon::HttpStatusCode::k404NotFound);
			}
			callback(resp);
		}

		void getByTerm(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback, const std::string& term) const {

			auto resp = drogon::HttpResponse::newHttpResponse();

			if(term.empty())
			{
				resp->setStatusCode(drogon::HttpStatusCode::k400BadRequest);
				callback(resp);
				return;
			}

			auto persons = this->_personService->getByTerm(term);

			Poco::JSON::Array personsJson;

			for (auto& person : (*persons))
			{
				auto personJson = PersonAdapter::toJsonObject(person);
				personsJson.add(personJson);
			}

			std::ostringstream outputJson;
			personsJson.stringify(outputJson);
			
			resp->setStatusCode(drogon::HttpStatusCode::k200OK);
			resp->setContentTypeCode(drogon::ContentType::CT_APPLICATION_JSON);
			resp->setBody(outputJson.str());
			callback(resp);
		}

		void count(const drogon::HttpRequestPtr &req, std::function<void(const drogon::HttpResponsePtr &)> &&callback) const {
			auto count = this->_personService->count();
			auto resp = drogon::HttpResponse::newHttpResponse();
			resp->setStatusCode(drogon::HttpStatusCode::k200OK);
			resp->setContentTypeCode(drogon::ContentType::CT_TEXT_PLAIN);
			resp->setBody(std::to_string(count));
			callback(resp);
		}

	public:
		PersonController(PersonServicePrt personService) : _personService(personService)
		{
		}

    private:
        PersonServicePrt _personService;
};
